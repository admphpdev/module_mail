<?php

namespace amd_php_dev\module_mail;

/**
 * mail module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Module
{
    use \amd_php_dev\yii2_components\modules\ComposerModuleTrait;
    
    //public $layout      = '@app/views/layouts/default';
    protected $_urlRules = [
        [
            'rules' => [
                ['class' => '\amd_php_dev\module_mail\urlRules\Rules']
            ],
            'append' => true,
        ],
    ];
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'amd_php_dev\module_mail\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //$this->modules = [
        //
        //];

        // custom initialization code goes here
    }

    //public static function getMenuItems() {
    //    return [
    //        'section' => 'mail',
    //        'items' => [
    //            [
    //                'label' => 'mail',
    //                'items' => [
    //                    ['label' => 'label', 'url' => ['']],
    //                ]
    //            ]
    //        ],
    //    ];
    //}
}
