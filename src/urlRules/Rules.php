<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 05.11.2016
 * Time: 21:06
 */

namespace amd_php_dev\module_mail\urlRules;

use yii\web\UrlRuleInterface;
use yii\base\Object;

class Rules extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if ($request->hostName != \yii::$app->params['HOST']) {
            return false;
        }

        if (preg_match("/^mail\/(:?([\S]+))?$/", $pathInfo, $matches)) {
            $action = !empty($matches[2]) ? $matches[2] : 'index';

            return ['mail/default/' . $action, []];
        }
        
        return false;
    }
}