<?php

namespace amd_php_dev\module_mail\controllers;

use yii\web\Controller;

/**
 * Default controller for the `mail` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCallback()
    {
        $data = [
            'params' => \yii::$app->request->post(),
            'message' => 'Почта еще не настроена'
        ];
        $this->sendResult(0, $data);
    }

    protected function sendResult($status, $data = '')
    {
        $result = [
            'status' => $status,
            'data' => $data
        ];

        echo json_encode($result);
        \yii::$app->end();
    }
}
